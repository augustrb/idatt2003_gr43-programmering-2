# ChaosGame

## Description
ChaosGame is a Java application that visualizes different fractals using the chaos game method. It includes various transformations such as Julia sets and the Sierpinsky Triangle.

## Features
- Visualize different fractals including Julia sets and the Dragon curve.
- Control the number of iterations to see how the fractal evolves.
- Save a self made tranformation to a file and load it later.

## Installation
This project uses Maven for dependency management. To install and run the project:

1. Clone the repository: `git clone git@gitlab.stud.idi.ntnu.no:augustrb/idatt2003_gr43-programmering-2.git`
2. Navigate to the project directory: `cd ChaosGame`
3. Build the project: `mvn clean install`
4. Run the project: `mvn javafx:run`

## Usage
After running the project, select the desired fractal from the available options. Adjust the number of iterations and click the button to visualize the fractal. You can also save a transformation to a file and load it later.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
